ublock-origin (1.62.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.62.0+dfsg.

 -- Markus Koschany <apo@debian.org>  Sun, 09 Feb 2025 19:20:16 +0100

ublock-origin (1.60.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.60.0+dfsg. (Closes: #1078943)

 -- Markus Koschany <apo@debian.org>  Wed, 02 Oct 2024 09:10:25 +0200

ublock-origin (1.57.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.57.0+dfsg.

 -- Markus Koschany <apo@debian.org>  Sat, 06 Apr 2024 16:19:34 +0200

ublock-origin (1.55.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.55.0+dfsg.

 -- Markus Koschany <apo@debian.org>  Sun, 21 Jan 2024 21:18:52 +0100

ublock-origin (1.54.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.54.0+dfsg.

 -- Markus Koschany <apo@debian.org>  Sun, 03 Dec 2023 19:00:28 +0100

ublock-origin (1.53.3rc0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.53.3rc0+dfsg. (Closes: #1055315)

 -- Markus Koschany <apo@debian.org>  Fri, 10 Nov 2023 05:24:39 +0100

ublock-origin (1.52.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.52.0+dfsg. (Closes: #1052658)

 -- Markus Koschany <apo@debian.org>  Wed, 27 Sep 2023 01:39:18 +0200

ublock-origin (1.51.0+dfsg-2) unstable; urgency=medium

  * Replace js-beautify and csstree Javascript libraries with system libraries.
    Document the licenses of js-beautify, csstree and hsluv in debian/copyright.
    Add the missing source file of hsluv.js to debian/missing-sources.
    Thanks to Bastien Roucariès for the report. (Closes: #1042757)

 -- Markus Koschany <apo@debian.org>  Sat, 19 Aug 2023 01:01:05 +0200

ublock-origin (1.51.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.51.0+dfsg. (Closes: #1039061)
  * Build the minified asset filters from source. Build-depend on nodejs.
  * d/control: Remove obsolete Breaks and Replaces.

 -- Markus Koschany <apo@debian.org>  Sat, 22 Jul 2023 00:43:04 +0200

ublock-origin (1.46.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.46.0+dfsg.
  * Declare compliance with Debian Policy 4.6.2.

 -- Markus Koschany <apo@debian.org>  Mon, 23 Jan 2023 22:02:37 +0100

ublock-origin (1.44.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.44.0+dfsg. (Closes: #1016901)
  * Drop transitional webext-ublock-origin binary package.
  * Declare compliance with Debian Policy 4.6.1.
  * Remove lz4-block-codec.wat.patch and python3.patch. Fixed upstream.

 -- Markus Koschany <apo@debian.org>  Tue, 23 Aug 2022 00:42:54 +0200

ublock-origin (1.42.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.42.0+dfsg.
    - Fix FTBFS unexpected token get_local. (Closes: #1005502)

 -- Markus Koschany <apo@debian.org>  Fri, 06 May 2022 03:17:53 +0200

ublock-origin (1.40.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.40.2+dfsg.

 -- Markus Koschany <apo@debian.org>  Thu, 30 Dec 2021 23:12:02 +0100

ublock-origin (1.39.0+dfsg-2) unstable; urgency=medium

  * Fix debian/watch to detect new upstream releases.

 -- Markus Koschany <apo@debian.org>  Fri, 26 Nov 2021 23:10:11 +0100

ublock-origin (1.39.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.39.0+dfsg.
  * Skip the tests.

 -- Markus Koschany <apo@debian.org>  Fri, 26 Nov 2021 22:34:21 +0100

ublock-origin (1.37.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.37.0+dfsg.
    - Fix CVE-2021-36773:
      uBlock supported an arbitrary depth of parameter nesting for strict
      blocking, which allows crafted web sites to cause a denial of service
      (unbounded recursion that can trigger memory consumption and a loss of
      all blocking functionality).
      Thanks to Marcus Frings for the report. (Closes: #991386)
  * Declare compliance with Debian Policy 4.6.0.

 -- Markus Koschany <apo@debian.org>  Wed, 18 Aug 2021 20:56:50 +0200

ublock-origin (1.33.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.33.0+dfsg.

 -- Markus Koschany <apo@debian.org>  Tue, 16 Feb 2021 00:10:01 +0100

ublock-origin (1.32.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.32.0+dfsg.
  * Declare compliance with Debian Policy 4.5.1.

 -- Markus Koschany <apo@debian.org>  Sat, 26 Dec 2020 20:48:03 +0100

ublock-origin (1.30.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.30.0+dfsg.

 -- Markus Koschany <apo@debian.org>  Sun, 04 Oct 2020 15:43:32 +0200

ublock-origin (1.29.0+dfsg-2) unstable; urgency=medium

  * Update debian/copyright. Readd codemirror license which got accidentally
    removed and two new fonts licenses. (Closes: #969742)

 -- Markus Koschany <apo@debian.org>  Tue, 08 Sep 2020 01:16:48 +0200

ublock-origin (1.29.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.29.0+dfsg.
    - Restore compatibility with Firefox 80. (Closes: #969123)
  * Restore compatibility with Chromium. Add new binary package
    webext-ublock-origin-chromium. (Closes: #954097)

 -- Markus Koschany <apo@debian.org>  Thu, 27 Aug 2020 23:43:42 +0200

ublock-origin (1.28.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.28.0+dfsg. (Closes: #952645)
  * Remove obsolete binary packages xul-ext-ublock-origin and
    chromium-ublock-origin. (Closes: #908158)
  * Install the Firefox version into webext-ublock-origin. Temporarily only the
    Firefox web browser is supported. A new version of ublock-origin is
    currently waiting in the ftp-master's NEW queue. As soon as this version
    enters Debian, both web browsers, Chromium and Firefox, will be supported
    again.
  * Fix the wrong version number in both Firefox and Chromium manifest files.
    (Closes: #946796)

 -- Markus Koschany <apo@debian.org>  Sat, 25 Jul 2020 17:26:09 +0200

ublock-origin (1.22.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.22.2+dfsg.

 -- Markus Koschany <apo@debian.org>  Sun, 15 Sep 2019 00:13:54 +0200

ublock-origin (1.19.0+dfsg-2) unstable; urgency=medium

  * Upload to unstable.
  * Declare compliance with Debian Policy 4.4.0.

 -- Markus Koschany <apo@debian.org>  Sun, 07 Jul 2019 23:57:21 +0200

ublock-origin (1.19.0+dfsg-1) experimental; urgency=medium

  [ Michael Meskes ]
  * Change package layout to allow for different file for each browser while at
    the same time keeping firefox working despite its dislike for symlinks.
    (Closes: #926586)

  [ Markus Koschany ]
  * New upstream version 1.19.0+dfsg.

 -- Markus Koschany <apo@debian.org>  Sun, 19 May 2019 19:30:24 +0200

ublock-origin (1.18.10+dfsg-1) experimental; urgency=medium

  [ Markus Koschany ]
  * New upstream version 1.18.10+dfsg.
  * Fix ublock-origin being disabled with Firefox 66. (Closes: #925337)
  * Switch to compat level 12.

 -- Markus Koschany <apo@debian.org>  Sat, 23 Mar 2019 22:54:24 +0100

ublock-origin (1.18.4+dfsg-2) unstable; urgency=medium

  * Remove /etc/chromium.d/ublock-origin on upgrade because this file is
    obsolete. (Closes: #923001)

 -- Markus Koschany <apo@debian.org>  Sun, 24 Feb 2019 15:51:26 +0100

ublock-origin (1.18.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.18.4.
  * Remove vapi-webrequest.patch. Fixed upstream.
  * Drop 0004-patch-README-for-Debian.patch and do not install README.md.

  [ Michael Meskes ]
  * Remove debian/chromium/* since Chromium will load all extensions now.

 -- Markus Koschany <apo@debian.org>  Wed, 06 Feb 2019 11:34:39 +0100

ublock-origin (1.18.2+dfsg-2) unstable; urgency=medium

  * Upload to unstable.
  * Drop do-not-open-sidebar-on-first-start.patch. Fixed upstream.
  * Reuse both flavors of webRequest wrapper in webext package.
    Thanks to Raymond Hill for the patch. (Closes: #920652)

 -- Markus Koschany <apo@debian.org>  Mon, 28 Jan 2019 23:14:43 +0100

ublock-origin (1.18.2+dfsg-1) experimental; urgency=medium

  * New upstream version 1.18.2 DFSG-cleaned.
  * Declare compliance with Debian Policy 4.3.0.
  * Remove debian/missing-sources again because upstream provides the sources.
  * Compile all *.wat files from source now.

 -- Markus Koschany <apo@debian.org>  Sun, 27 Jan 2019 17:08:27 +0100

ublock-origin (1.17.0+dfsg-3) unstable; urgency=medium

  * Replace symlink to fontawesome-webfont with a real file again. Firefox
    silently ignored this symlink and icons were not displayed. This also fixes
    the logger window which was empty before. (Closes: #916431, #906911)

 -- Markus Koschany <apo@debian.org>  Sat, 15 Dec 2018 14:24:36 +0000

ublock-origin (1.17.0+dfsg-2) unstable; urgency=medium

  * Remove the quotation marks around boolean value in
    do-not-open-sidebar-on-first-start.patch. That prevented Firefox from
    loading the addon. Thanks to Eugen Dedu for the report.
    (Closes: #910807)

 -- Markus Koschany <apo@debian.org>  Thu, 11 Oct 2018 17:14:14 +0200

ublock-origin (1.17.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.17.0+dfsg.
  * Update upstream changelog to the new release version.
  * Drop make-webext-meta-encoding.patch and make-webext.patch. Fixed upstream.
  * Add do-not-open-sidebar-on-first-start.patch and prevent that the sidebar
    in Firefox opens on first startup. This feature only works in
    Firefox >= 62. (Closes: #909493)
  * Fix debian/watch and only track relevant upstream versions. Thanks to Sven
    Joachim for the report and patch. (Closes: #908898)

 -- Markus Koschany <apo@debian.org>  Wed, 10 Oct 2018 11:28:12 +0200

ublock-origin (1.16.14+dfsg-2) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.2.1.
  * Build-depend on python3 and fix that python commands were silently ignored
    in make-webext.sh.
    Thanks to Laurent Bigonville for the report. (Closes: #908509)
  * Add make-webext.patch. Call bash with set -e and exit when a command exits
    with a non-zero status. Also use the python3 executable instead of the
    default python interpreter.
  * Add make-webext-meta-encoding.patch and fix an encoding issue and fatal
    error that would cause a FTBFS.

 -- Markus Koschany <apo@debian.org>  Mon, 10 Sep 2018 19:26:50 +0200

ublock-origin (1.16.14+dfsg-1) unstable; urgency=medium

  * New upstream version 1.16.14+dfsg-1.
  * Add myself to Uploaders and remove Sean Whitton at his own request.
    (Closes: #877041)
  * Drop 0005-Fix-application-id.patch. Fixed upstream. Thanks to Laurent
    Bigonville for the previous fix.
  * Suggest ublock-origin-doc package. (Closes: #880533)
  * Install the transition script ublock_migration.sh into
    /usr/share/doc/webext-ublock-origin. Add a NEWS file and explain how users
    can transition their data from the XUL extension to the new webext.
    Thanks to david s for providing the script! (Closes: #877040)
  * Declare compliance with Debian Policy 4.2.0.

 -- Markus Koschany <apo@debian.org>  Fri, 10 Aug 2018 08:18:58 +0200

ublock-origin (1.16.6+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix the incorrect value for applications.gecko.id in
    platform/webext/manifest.json, this makes the extension work again with
    firefox (Closes: #899365)

 -- Laurent Bigonville <bigon@debian.org>  Thu, 12 Jul 2018 13:00:00 +0200

ublock-origin (1.16.6+dfsg-1) unstable; urgency=medium

  [ Sean Whitton ]
  * Build and install webextension in new binary package webext-ublock-
    origin.

  [ Michael Meskes ]
  * New upstream version.
  * Switch to webext package
  * Updated VCS information.
  * Bumped standards and debhelper version

 -- Michael Meskes <meskes@debian.org>  Mon, 09 Apr 2018 20:15:53 +0200

ublock-origin (1.13.8+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh 0004-patch-README-for-Debian.patch

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 30 Jul 2017 18:22:57 -0700

ublock-origin (1.13.6+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 02 Jul 2017 21:52:05 +0100

ublock-origin (1.12.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards version to 4.0.0 (no changes required).
  * Refresh 0001-Disable-nonfree-filters-by-default.patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 19 Jun 2017 11:34:01 +0100

ublock-origin (1.11.4+dfsg-2) experimental; urgency=high

  * Add missing Breaks/Replaces of ublock-origin-doc (Closes: #859569).
    Thanks Andreas Beckmann for reporting this error.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 04 Apr 2017 16:13:19 -0700

ublock-origin (1.11.4+dfsg-1) experimental; urgency=medium

  * New upstream release.
  * Move larger media files to new -doc package (Closes: #857797).
  * Build the Chromium extension (Closes: #858526).
    Thanks to James McCoy for the patch.
    - New binary package: chromium-ublock-origin.
    - Update 0003-patch-make-assets.sh-for-Debian.patch.
  * Extend 0001-Disable-nonfree-filters-by-default.patch to apply to new
    file assets/assets.json.
  * Refresh other patches.
  * Bump debhelper compat & build-dep to 10.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 22 Mar 2017 22:17:30 -0700

ublock-origin (1.10.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh 0004-patch-README-for-Debian.patch

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 09 Jan 2017 21:34:11 -0700

ublock-origin (1.10.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Remove references to Conkeror support from README.source.
    Upstream has closed feature request as wontfix.
  * Quilt patch changes:
    - Refresh 0001-Disable-nonfree-filters-by-default.patch
    - Drop 0002-Disable-conkeror-support.patch
      Merged upstream.
  * /usr/share/doc/xul-ext-ublock-origin changes:
    - Rename doc/benchmarks => benchmarks
    - Rename doc/media => media
    - Stop installing doc/img/: these are for upstream's wiki.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 30 Nov 2016 13:30:13 -0700

ublock-origin (1.9.16+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 27 Oct 2016 21:28:56 -0700

ublock-origin (1.9.12+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 08 Oct 2016 17:55:00 -0700

ublock-origin (1.9.10+dfsg-2) unstable; urgency=medium

  * Install an absolute symlink to fontawesome-webfont.ttf.
    See comment in d/rules for justification.
    Thanks to all those who contributed to finding this fix, in #827517.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 02 Oct 2016 10:49:38 -0700

ublock-origin (1.9.10+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh 0001-Disable-nonfree-filters-by-default.patch.
  * Add 0004-patch-README-for-Debian.patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 27 Sep 2016 10:51:30 -0700

ublock-origin (1.9.6+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Remove deleted assets/.../effective_tld_names.dat from d/copyright.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 15 Sep 2016 10:48:33 -0700

ublock-origin (1.9.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright file for third party assets moved into uAssets/.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 05 Sep 2016 08:32:11 -0700

ublock-origin (1.8.4+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 08 Aug 2016 06:30:42 -0700

ublock-origin (1.8.2+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 06 Aug 2016 08:55:43 -0700

ublock-origin (1.8.0+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 05 Aug 2016 09:23:11 -0700

ublock-origin (1.7.6+dfsg-1) unstable; urgency=medium

  * New upstream version.
  * Improve README.source.
    - Move `git diff` cmd into ordered list.
    - Add "upload to ftp-master" step.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 26 Jun 2016 09:40:08 +0900

ublock-origin (1.7.0+dfsg-1) unstable; urgency=medium

  * Package new upstream release.
  * Add a missing step to README.source.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 02 May 2016 11:39:52 -0700

ublock-origin (1.6.8+dfsg-1) unstable; urgency=medium

  * Package new upstream release.
  * Explicitly include copyright for Raymond Hill.
  * Updates for upstream's repository reorganisation:
    - Patch tools/make-assets.sh
    - Update debian/clean-dfsg.sh
    - Update debian/copyright
    - Explain changes README.source.
  * Bump standards version to 3.9.8 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 18 Apr 2016 20:38:22 -0700

ublock-origin (1.6.6+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh non-free filters patch.
  * Add a patch disabling Conkeror support pending a fix of upstream bug
    #1356.  (Closes: #816896)
  * Bump standards version to 3.9.7; no changes required.
  * More commentary in README.source.
  * Update copyright file in line with upstream.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 29 Mar 2016 14:34:11 -0700

ublock-origin (1.6.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
    Remove now superfluous Lintian overrides.
  * Update patch disabling non-free filters by default for new upstream
    release.
  * Improve commentary in README.source.
  * Generate an upstream changelog using amo-changelog.
  * Add justifications for remaining Lintian overrides.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 15 Feb 2016 08:05:37 -0700

ublock-origin (1.5.6+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #778719)

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 12 Jan 2016 18:35:50 -0700
